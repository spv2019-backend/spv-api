export default (allowedColumns) => {
  return (req, res, next) => {
    const filters = Object.keys(req.query).filter((queryParam) => allowedColumns.includes(queryParam)).reduce((obj, item) => {
      obj[item] = req.query[item];
      return obj;
    }, {});
    req.filter = filters;
    next();
  }
}
