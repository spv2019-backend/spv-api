export default (allowedColumn) => {
  return (req, res, next) => {
    const query = req.query.q;
    if (query) {
      req.search = {
        [allowedColumn]: {
          $regex: query,
          $options: 'i'
        }
      }
    }
    next()
  }
}