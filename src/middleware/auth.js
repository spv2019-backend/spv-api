import jwt from 'jsonwebtoken';
import config from '../config';

export default (adminRestricted=false) => {
    return (req, res, next) => {
        req.authToken = req.headers.authorization;
        try {
            const decoded = jwt.verify(req.authToken, config.auth.authSecret);
            req.decodedAuthToken = decoded;
            const role = JSON.parse(decoded.user).role;
            if (adminRestricted && role !== 'admin') {
                return res.status(401).send({
                    error: 'Unauthorized'
                })
            }
        } catch (err) {
            return res.status(401).send({
                error: 'Unauthorized!'
            })
        }
        next();
    }
}

