import {Schema, model} from 'mongoose';

const CommentSchema = new Schema({
  authorId: { type: Schema.Types.ObjectId, ref: 'User'},
  author: Schema.Types.Object,
  text: String,
  replies: [ Schema.Types.Object ]
});

export default model('Comment', CommentSchema);