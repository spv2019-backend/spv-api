import {Schema, model} from 'mongoose';

const PostSchema = new Schema({
    title: String,
    description: String,
    authorId: {type: Schema.Types.ObjectId, ref: 'User'},
    author: Schema.Types.Object,
    hasRated: Boolean,
    rating: Number,
    replies: [{ type: Schema.Types.ObjectId, ref: 'Comment' }],
    courseId: { type: Schema.Types.ObjectId, ref: 'Course' },
    course: Schema.Types.Object,
    images: [String],
    files: [{
        fileExtension: {
            type: Schema.Types.String,
            required: true
        },
        fileName: {
            type: Schema.Types.String,
            required: true
        },
        downloadPath: {
            type: Schema.Types.String,
            required: true
        }
    }],
    createdAt: {
        type: Schema.Types.Date,
        required: true,
        default: Date.now
    }
});

export default model('Post', PostSchema);
