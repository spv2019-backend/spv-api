import {Schema, model} from 'mongoose';

const Roles = {
    ADMIN: 'admin',
    USER: 'user'
};

const UserSchema = new Schema({
    username: {
        type: Schema.Types.String,
        required: true,
        unique: true
    },
    password: {
        type: Schema.Types.String,
        required: true
    },
    email: {
        type: Schema.Types.String,
        required: true,
        unique: true
    },
    firstName: {
        type: Schema.Types.String,
        default: null
    },
    lastName: {
        type: Schema.Types.String,
        default: null
    },
    lastOnlineAt: {
        type: Schema.Types.Date,
        default: null
    },
    createdAt: {
        type: Schema.Types.Date,
        required: true,
        default: Date.now()
    },
    role: {
        type: Schema.Types.String,
        required: true,
        default: Roles.USER
    }
});

const User = model('User', UserSchema);

export default User;
