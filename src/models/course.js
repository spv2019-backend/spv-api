import {Schema, model} from 'mongoose';

const CourseSchema = new Schema({
    program: {type: Schema.Types.ObjectId, ref: 'Program'},
    acronym: String,
    name: String,
    year: Number,
    description: String,
    image: String,
    posts: [{type: Schema.Types.ObjectId, ref: 'Post'}],
    color: {
        type: Schema.Types.String,
        required: true,
        default: '#C0C0C0'
    }
});

export default model('Course', CourseSchema);
