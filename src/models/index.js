export { default as User } from './user';
export { default as Post } from './post';
export { default as Faculty } from './faculty';
export { default as Program } from './program';
export { default as Course } from './course';
export { default as Comment } from './comment';
