import { Schema, model } from 'mongoose';

const FacultySchema = new Schema({
  title: String,
  description: String,
  programs: [ { type: Schema.Types.ObjectId, ref: 'Program' } ],
  image: String,
  link: String
});

export default model('Faculty', FacultySchema);
