import { Schema, model } from 'mongoose';

const ProgramSchema = new Schema({
    faculty: { type: Schema.Types.ObjectId, ref: 'Faculty' },
    courses: [ { type: Schema.Types.ObjectId, ref: 'Course' } ],
    type: String
});

export default model('Program', ProgramSchema);