export default { 
  url: process.env.DOCS_URL || 'localhost',
  port: process.env.DOCS_PORT || 8080
}