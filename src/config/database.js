export default { 
  port: process.env.DATABASE_PORT || 27017,
  host: process.env.DATABASE_HOST || '127.0.0.1',
  databaseName: process.env.DATABASE_NAME || 'deljenjeZapiskov'
}