import express from './express';
import db from './database';
import auth from './auth';
import docs from './docs';

export default {
  express,
  db,
  auth,
  docs
}