import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import swaggerJsdoc from 'swagger-jsdoc'
import swaggerUi from 'swagger-ui-express';
import fs from 'fs';
import path from 'path';
import dotenv from 'dotenv-flow'
import cors from 'cors';
import morgan from 'morgan';

import routes from './routes';
import config from './config';

dotenv.config({
	default_node_env: 'development'
});

mongoose.connect(`mongodb+srv://shadowfiga:e5nupfwc@sipv-xs45y.mongodb.net/test?retryWrites=true`, {
	useNewUrlParser: true
});

const dbConnection = mongoose.connection;
dbConnection.on('error', console.error.bind(console, 'MongoDB connection error:'));

const app = express();
// Logging
app.use(morgan('tiny'));
// cors
app.use(cors());

if (!fs.existsSync('public')) {
	fs.mkdirSync('public');
}
const options = {
	swaggerDefinition: {
		info: {
			title: 'Deljenje zapiskov API description',
			version: '1.0.0',
		},
		host: `${config.docs.url}:${config.docs.port}`,
		basePath: '/api/v1',
		produces: ['application/json'],
		schemes: [
			'http'
		]
	},
	apis: [path.join(__dirname, './routes/**/index.js'), path.join(__dirname, './models/*.js'), path.join(__dirname, './middleware/*.js')],
};

const swaggerSpec = swaggerJsdoc(options);

app.use(bodyParser.urlencoded({
	extended: false
}));
app.use(bodyParser.json());
app.use('/api/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
app.use('/public', express.static(path.join(__dirname, '../public')));
console.log(path.join(__dirname, '../public'));
app.use('/api/v1', routes);

app.get('/api-docs.json', (req, res) => {
	res.setHeader('Content-Type', 'application/json');
	res.send(swaggerSpec);
});

app.listen((config.express.port), () => {
	console.log(`Listening on port ${config.express.port}`);
});
