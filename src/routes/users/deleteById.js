export default async (req, res) => {
  try {
    await req.user.remove();
  } catch (error) {
    console.log(error);
    return res.status(500).send({
      error: `Internal server error: ${error}`
    });
  }

  return res.status(200).send({
    success: `User with ID ${req.params.userId} has been deleted from the database.`
  });
}
