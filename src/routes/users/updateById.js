import bcrypt from 'bcrypt';

import { User } from '../../models';

export default async (req, res) => {
  if (Object.keys(req.body).length === 0)
    return res.status(400).send('User info was not provided.');

  const user = await User.findOne(req.id);
  const password = req.body.password ? await bcrypt.hash(req.body.password, 10) : undefined;

  const newUser = new User({
    _id: user._id,
    username: req.body.username,
    password: password,
    email: req.body.email,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
  })


  try {
    await User.findOneAndUpdate(req.id, newUser);
  } catch (error) {
    return res.status(500).send({
      error: `An internal server error has occured: ${error}`
    });
  }

  return res.status(200).send({ success: 'User info updated.' });
}
