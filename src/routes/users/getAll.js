import { User } from "../../models";

export default async (req, res) => {
  const users = await User.find()

  return res.send(users);
}