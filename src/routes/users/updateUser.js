import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

import {User} from '../../models';
import config from '../../config';

export default async (req, res) => {
    let user = {};
    try {
        user = await User.findOne({_id: req.decodedAuthToken.id});
        if (req.body.firstName) {
            user.firstName = req.body.firstName
        }
        if (req.body.lastName) {
            user.lastName = req.body.lastName
        }
        if (req.body.password) {
            let password = await bcrypt.hash(req.body.password, 10);
            user.password = password
        }
        await user.save()
    } catch (error) {
        return res.status(500).send({
            error: `An internal server error has occured: ${error}`
        });
    }

    return res.status(200).send({
        authToken: jwt.sign({
            id: user.id,
            user: JSON.stringify(user),
        }, config.auth.authSecret, {expiresIn: '1d'}),
        user: {
            "id": user.id,
            "email": user.email,
            "username": user.username,
            "firstName": user.firstName,
            "lastName": user.lastName,
            "role": user.role
        }
    });
}
