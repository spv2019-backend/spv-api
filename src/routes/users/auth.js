import checkAPI from 'express-validator/check';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

import {User} from '../../models';
import config from '../../config';

export default async (req, res) => {
    const validationErrors = checkAPI.validationResult(req);

    if (!validationErrors.isEmpty()) {
        return res.status(422).send({
            errors: validationErrors.array()
        });
    }

    try {
        const user = await User.findOne({
            username: req.body.username
        });
        let token;
        try {
            const verified = await bcrypt.compare(req.body.password, user.password);
            if (verified) {

                token = jwt.sign({
                    id: user.id,
                    user: JSON.stringify(user)
                }, config.auth.authSecret, {expiresIn: '1d'});

            } else {
                return res.status(401).send({
                    error: 'Wrong password'
                })
            }
        } catch (err) {
            if (err) {
                console.log(err);
                return res.status(401).send({
                    error: 'Wrong password!'
                });
            }
        }
        return res.send({
            authToken: token,
            user: {
                id: user.id,
                email: user.email,
                username: user.username,
                firstName: user.firstName,
                lastName: user.lastName,
                role: user.role
            }
        })
    } catch (err) {
        return res.status(500).send({
            error: 'Internal server error'
        })
    }

}
