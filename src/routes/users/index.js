import {
  Router
} from 'express';
import checkAPI from 'express-validator/check';

import getAll from './getAll';
import getById from './getById';
import deleteById from './deleteById';
import {
  User
} from '../../models';
import register from './register';
import getUsersPosts from './getUsersPosts';
import authRoute from './auth';
import updateById from './updateById';
import auth from '../../middleware/auth';
import updateUser from './updateUser';

const users = Router();

users.param('userId', async (req, res, next) => {
  const user = await User.findById(req.params.userId);

  if (user) {
    req.user = user;
    next();
  } else {
    return res.status(404).send({
      error: `User with ID ${req.params.userId} does not exist in the database.`
    })
  }
});

/**
 * @swagger 
 * /users: 
 *   get: 
 *    description: Returns an array of Users.
 *    tags: 
 *      - users 
 *    produces: 
 *      - application/json
 *    responses: 
 *      200: 
 *       description: OK
 *       schema: 
 *        type: array
 *        items:
 *          $ref: '#/definitions/User'
 *      500: 
 *       description: An internal server error has occured. Contact the administrator.
 */
users.get('/', getAll);
/**
 * @swagger 
 * /users/{id}: 
 *   get: 
 *    description: Returns a single User with `id` if it exists.
 *    parameters:
 *      - name: id
 *        in: path
 *        description: ID of the User.
 *        required: true
 *        type: string
 *    tags: 
 *      - users 
 *    produces: 
 *      - application/json
 *    responses: 
 *      200: 
 *       description: OK
 *       schema: 
 *        type: object
 *        $ref: '#/definitions/User'
 *      404: 
 *       description: User with the specified `id` does not exist.
 *      500: 
 *       description: An internal server error has occured. Contact the administrator.
 */
users.get('/:userId', auth(), getById);
/**
 * @swagger 
 * /users/{id}/posts: 
 *   get: 
 *    description: Returns an array of Posts related to User with `id`.
 *    parameters:
 *      - name: id
 *        in: path
 *        description: ID of the User.
 *        required: true
 *        type: string
 *    tags: 
 *      - users 
 *    produces: 
 *      - application/json
 *    responses: 
 *      200: 
 *       description: OK
 *       schema: 
 *        type: array
 *        items:
 *          $ref: '#/definitions/User'
 *      404: 
 *       description: User with the specified `id` does not exist.
 *      500: 
 *       description: An internal server error has occured. Contact the administrator.
 */
users.get('/:userId/posts', auth(), getUsersPosts);

/**
 * @swagger 
 * /users/{id}: 
 *   delete: 
 *    description: Deletes a single User with `id` if it exists.
 *    parameters:
 *      - name: id
 *        in: path
 *        description: ID of the User.
 *        required: true
 *        type: string
 *    tags: 
 *      - users
 *    produces: 
 *      - application/json
 *    responses: 
 *      200: 
 *       description: OK
 *      404: 
 *       description: User with the specified `id` does not exist.
 *      500: 
 *       description: An internal server error has occured. Contact the administrator.
 */
users.delete('/:userId', deleteById);

/**
 * @swagger 
 * /users/register: 
 *   post: 
 *    description: Registers a User.
 *    parameters:
 *      - name: user
 *        description: A new User.
 *        in: body
 *        required: true
 *        schema:
 *          $ref: '#/definitions/NewUser'
 *    tags: 
 *      - users
 *    produces: 
 *      - application/json
 *    responses: 
 *      201: 
 *       description: User added.
 *      400: 
 *       description: Bad request.
 *      500: 
 *       description: An internal server error has occured. Contact the administrator.
 */
users.post('/register', [
  checkAPI.check('username').isString(),
  checkAPI.check('password').isLength({
    min: 6
  }),
  checkAPI.check('email').isEmail(),
  checkAPI.check('firstName').exists(),
  checkAPI.check('lastName').exists()
], register);
/**
 * @swagger 
 * /users/login: 
 *   post: 
 *    description: Logs in a User.
 *    parameters:
 *      - name: user
 *        description: Users credentials
 *        in: body
 *        required: true
 *        schema:
 *          $ref: '#/definitions/LoginUser'
 *    tags: 
 *      - users
 *    produces: 
 *      - application/json
 *    responses: 
 *      200: 
 *       description: User logged in.
 *      400: 
 *       description: Bad request.
 *      500: 
 *       description: An internal server error has occured. Contact the administrator.
 */
users.post('/login', [
  checkAPI.check('username').isString(),
  checkAPI.check('password').isLength({
    min: 6
  })
], authRoute);

/**
 * @swagger 
 * /users: 
 *   put: 
 *    description: Updates a User.
 *    parameters:
 *      - name: user
 *        description: Fields to update
 *        in: body
 *        required: true
 *        schema:
 *          $ref: '#/definitions/User'
 *    tags: 
 *      - users
 *    produces: 
 *      - application/json
 *    responses: 
 *      200: 
 *       description: User updated.
 *      400: 
 *       description: Bad request.
 *      500: 
 *       description: An internal server error has occured. Contact the administrator.
 */
users.put('/:userId', updateById);
users.put('/', [
  checkAPI.check('firstName').optional().isString(),
  checkAPI.check('lastName').optional().isString(),
  checkAPI.check('password').optional().isLength({
    min: 6
  })
], auth(), updateUser)

export default users;
