import checkAPI from 'express-validator/check';
import bcrypt from 'bcrypt';

import {User} from "../../models";
const Roles = {
    ADMIN: 'admin',
    USER: 'user'
};

export default async (req, res) => {
    const validationErrors = checkAPI.validationResult(req);

    if (!validationErrors.isEmpty()) {
        return res.status(422).send({
            errors: validationErrors.array()
        });
    }

    const password = await bcrypt.hash(req.body.password, 10);

    const newUser = new User({
        username: req.body.username,
        password: password,
        email: req.body.email,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        role: req.body.token === 'someamazingadmintoken' ? Roles.ADMIN : Roles.USER,
            createdAt : new Date()
    });

    try {
        await newUser.save();
    } catch (error) {
        return res.status(405).send({
            error: `An internal server error has occured: ${error.message}`
        });
    }

    return res.status(201).send({
        success: 'User registered!'
    });
}
