import { User } from "../../models";
import { Post } from "../../models";

export default async (req, res) =>
{

  Post.find({ author: req.params.userId }, function(err, posts)
  {

     if (err)
     {
        return res.send(err);
     }

     if(!posts.length)
     {
        return res.send(`No posts from user with id ${req.params.userId}.`);
     }
     else
     {
        return res.send(posts);
     }


  });


}
