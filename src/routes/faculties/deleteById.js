export default async (req, res) => {
  try {
    await req.faculty.remove();
  } catch(err) {
    console.log(err);
    return res.status(500).send({
      error: 'Internal server error'
    });
  }

  return res.status(204).send();
}