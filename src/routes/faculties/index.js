import { Router } from 'express';
import checkAPI from 'express-validator/check';

import getAll from './getAll';
import { Faculty } from '../../models';
import getById from './getById';
import create from './create';
import deleteById from './deleteById';
import updateById from './updateById';
import search from '../../middleware/search';

// TODO 204 za put, post, delete
const faculties = Router();

faculties.param('facultyId', async (req, res, next) => {
  const faculty = await Faculty.findOne({
    _id: req.params.facultyId
  })

  if (faculty) {
    req.faculty = faculty;
    next();
  } else {
    return res.status(404).send({
      error: `Faculty with ID ${req.params.facultyId} does not exist in the database.`
    });
  }
});

faculties.get('/', search('title'), getAll);
faculties.get('/:facultyId', getById);

faculties.post('/', [
  checkAPI.check('title').isString(),
  checkAPI.check('description').isString(),
  checkAPI.check('image').optional().isString(),
  checkAPI.check('link').optional().isURL(),
], create);

faculties.delete('/:facultyId', deleteById);

faculties.put('/:facultyId', [
  checkAPI.check('title').optional().isString(),
  checkAPI.check('description').optional().isString(),
  checkAPI.check('programs').optional().isArray(),
  checkAPI.check('link').optional().isURL(),
  checkAPI.check('image').optional().isString()
], updateById);

export default faculties;