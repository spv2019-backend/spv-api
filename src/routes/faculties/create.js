import checkAPI from 'express-validator/check';

import { Faculty } from "../../models";

export default async (req, res) => {
  const validationErrors = checkAPI.validationResult(req);

  if (!validationErrors.isEmpty()) {
    return res.status(422).send({
      errors: validationErrors.array()
    });
  }

  const faculty = new Faculty(req.body);
  try {
    await faculty.save()
  } catch (err) {
    return res.status(500).send({
      error: 'Internal server error'
    });
  }
  return res.status(204).send();
}