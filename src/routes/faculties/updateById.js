import checkAPI from 'express-validator/check';

import { Faculty } from "../../models";

export default async (req, res) => {
  const validationErrors = checkAPI.validationResult(req);

  if (!validationErrors.isEmpty()) {
    return res.status(422).send({
      errors: validationErrors.array()
    });
  }

  try {
    await Faculty.updateOne({
      _id: req.faculty._id
    }, req.body);
  } catch (err) {
    console.log(err);
    return res.status(500).send({
      error: 'Internal server error occured'
    });
  }

  return res.status(204).send();
}