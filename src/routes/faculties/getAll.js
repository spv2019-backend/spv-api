import { Faculty } from '../../models';

export default async (req, res) => {
  const faculties = await Faculty.find(req.search || null).populate({
    path: 'programs',
    populate: {
      path: 'courses'
    }
  });
  return res.send(faculties);
}
