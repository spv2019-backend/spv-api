import { Router } from 'express';

import users from './users';
import posts from './posts';
import faculties from './faculties';
import programs from './programs';
import courses from './courses';
import files from './files';

const routes = Router();

routes.get('/', (req, res) => {
  res.send({ response: 'Hello world!' });
});

routes.use('/users', users);
routes.use('/posts', posts);
routes.use('/faculties', faculties);
routes.use('/programs', programs);
routes.use('/courses', courses);
routes.use('/files', files);

export default routes;
