import { Router } from 'express';
import multer from 'multer';
import fs from 'fs';
import {Post} from "../../models";
import posts from "../posts";
import auth from '../../middleware/auth';
const path = require('path');

const router = Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        if (!fs.existsSync('public')) {
            fs.mkdirSync('public');
        }
        if (!fs.existsSync('public/files')) {
            fs.mkdirSync('public/files');
        }
        req.file = JSON.parse(JSON.stringify(file));
        cb(null, 'public/files');
    },
    filename: (req, file, cb) => {
        cb(null, `${Date.now()}-${file.originalname}`);
    }
});

const upload = multer({ storage }).single('file');

router.param('postId', async (req, res, next) => {
    const post = await Post.findById(req.params.postId);

    if (post) {
        req.post = post;
        next();
    } else {
        return res.status(404).send({
            error: `Post with ID ${req.params.postId} does not exist in the database.`
        });
    }
});

router.post('/upload/:postId', auth(), async (req, res) => {
    await upload(req, res, async err => {
       if(err) {
           res.status(405).send()
       } else {
           try {
               const post = await Post.findOne({_id: req.params.postId, authorId: req.decodedAuthToken.id});
               await Post.findOneAndUpdate({_id: req.params.postId, authorId: req.decodedAuthToken.id}, {
                   files: [...post.files, {
                       fileExtension: '.' + req.file.filename.split('.').pop(),
                       fileName: req.file.originalname,
                       downloadPath: req.file.destination + '/' + req.file.filename
                   }]
               }, err => {
                   if(err) {
                       res.status(401).send({error: 'Unauthorized'})
                   } else {
                       res.status(204).send()
                   }
               })
           } catch (e) {
               console.log(e.message);
               res.status(500).send({error: e.message})
           }
       }
    });
});

router.delete('/remove/:postId/:fileId', auth(), async (req, res) => {
    const post = await Post.findOne({_id: req.params.postId, authorId: req.decodedAuthToken.id});
    const files = [];
    post.files.forEach(f => {
        if(f._id.toString() !== req.params.fileId) {
            files.push(f);
        }
    });
    const postUpdated = await Post.findByIdAndUpdate(post._id, {
        files
    }, err => {
        if(err) {
            return res.status(401).send({error: 'Unauthorized'})
        }
    });
    return res.status(200).send(postUpdated);
});

export default router;
