import { Router } from 'express';
import checkAPI from 'express-validator/check';

import getAll from './getAll';
import { Course } from '../../models';
import getById from './getById';
import create from './create';
import auth from '../../middleware/auth';

const courses = Router();

courses.param('courseId', async (req, res, next) => {
  const course = await Course.findOne({
    _id: req.params.courseId
  })

  if (course) {
    req.course = course;
    next();
  } else {
    return res.status(404).send({
      error: `Course with ID ${req.params.courseId} does not exist in the database.`
    });
  }
});

courses.get('/', getAll);
courses.get('/:courseId', getById);

courses.post('/', [
  checkAPI.check('acronym').exists(),
  checkAPI.check('name').exists(),
  checkAPI.check('year').exists(),
  checkAPI.check('description').exists(),
], auth(true), create);

export default courses;
