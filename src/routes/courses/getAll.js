import { Course } from "../../models";

export default async (req, res) => {
  const courses = await Course.find();
  return res.send(courses)
}