import {Course} from "../../models";
import checkAPI from 'express-validator/check';

export default async (req, res) => {
    const validationErrors = checkAPI.validationResult(req);

    if (!validationErrors.isEmpty()) {
        return res.status(422).send({
            errors: validationErrors.array()
        });
    }

    try {
        const course = new Course({
            acronym: req.body.acronym,
            name: req.body.name,
            year: req.body.year,
            description: req.body.description,
            color: req.body.color,
            posts: []
        });
        await course.save();
    } catch (err) {
        return res.status(500).send({
            error: 'Internal server error'
        });
    }
    return res.status(201).send({
        success: 'COurse added'
    })
}
