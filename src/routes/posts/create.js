import checkAPI from 'express-validator/check';
import { Post, Course } from '../../models';

export default async (req, res) => {
    const newPost = new Post({
        title: req.body.title,
        description: req.body.description,
        authorId: req.decodedAuthToken.id,
        author: JSON.parse(req.decodedAuthToken.user),
        replies: [],
        rating: 0,
        hasRated: false,
        courseId: req.body.courseId,
        // images: (req.files && req.files.images) && req.files.images.map((image) => `posts/${image.filename}`),
        // files: (req.files && req.files.files) && req.files.files.map((file) => `posts/${file.filename}`)
    });
    try {
        const post = await newPost.save();
        const course = await Course.findOne({ _id: post.courseId });
        course.posts.push(post._id);
        await course.save();
        post.course = course;
        await post.save();
        return res.json(post);
    } catch (error) {
        await post.delete();
        return res.status(500).send({
            error: `An internal server error has occured: ${error}`
        });
    }
}
