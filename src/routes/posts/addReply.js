
import { Comment } from '../../models';

export default async (req, res) => {
    let reply = new Comment({
        authorId: req.decodedAuthToken.id,
        author: JSON.parse(req.decodedAuthToken.user),
        text: req.body.text,
        replies: []
    });
    console.log(req.body.text);
    try {
        reply = await reply.save();
        const comment = await Comment.findOne({ _id: req.params.commentId  })
        comment.replies.push(reply.id)
        await comment.save()
    } catch (error) {
        return res.status(500).send({
            error: `An internal server error has occured: ${error}`
        });
    }

    return res.status(201).send({
        success: 'Comment added'
    })
}
