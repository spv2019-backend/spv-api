import {Course, Post} from '../../models';

export default async (req, res) => {
  try {
    const post = await Post.findOne({_id: req.params.postId});
    if (post.authorId == req.decodedAuthToken.id) {
      await Post.findOneAndUpdate({ _id: req.params.postId}, {
        // images: req.files.images ? req.files.images.map((image) => `posts/${image.filename}`) : post.images,
        // files: req.files.files ? req.files.files.map((file) => `posts/${file.filename}`) : post.files
          title: req.body.title,
          description: req.body.description,
          courseId: req.body.courseId ? req.body.courseId : post.courseId,
          course: await Course.findOne({_id: req.body.courseId ? req.body.courseId : post.courseId})
      })
    } else {
      return res.status(401).send({
        error: 'Unauthorized'
      })
    }
  } catch (error) {
    return res.status(500).send({
      error: `An internal server error has occured: ${error}`
    });
  }

  return res.status(200).send({ success: 'Post info updated.' });
}
