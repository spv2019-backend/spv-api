import checkAPI from 'express-validator/check';
import { Post, Course, Comment } from '../../models';

export default async (req, res) => {
  let comment = new Comment({
    authorId: req.decodedAuthToken.id,
    author: JSON.parse(req.decodedAuthToken.user),
    text: req.body.text,
    replies: []
  })
  try {
    comment = await comment.save();
    const post = await Post.findOne({ _id: req.params.postId })
    post.replies.push(comment.id)
    await post.save()
  } catch (error) {
    return res.status(500).send({
      error: `An internal server error has occured: ${error}`
    });
  }

  return res.status(201).send({
    success: 'Comment added'
  })
}
