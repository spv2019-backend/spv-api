import {Comment} from '../../models';

export default async (req, res) => {
    try {
        await Comment.findOneAndUpdate({ _id: req.params.commentId, authorId: req.decodedAuthToken.id}, {
                // images: req.files.images ? req.files.images.map((image) => `posts/${image.filename}`) : post.images,
                // files: req.files.files ? req.files.files.map((file) => `posts/${file.filename}`) : post.files
                text: req.body.text
        },
            err => {
                if(!err) {
                    res.status(204).send();
                } else {
                    res.status(401).send({status: 'Unauthorized'});
                }
            });
    } catch (error) {
        return res.status(500).send({
            error: `An internal server error has occured: ${error}`
        });
    }
}
