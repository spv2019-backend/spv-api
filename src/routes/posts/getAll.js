import { Post } from '../../models';

export default async (req, res) => {
  const posts = await Post.find();
  return res.send(posts);
}
