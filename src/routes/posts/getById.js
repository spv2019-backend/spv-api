import { Post } from '../../models';

export default async (req, res) => {
  try {
    const post = await Post.findOne({_id: req.params.postId}).populate('replies');
    if(!post){
      return res.status(404).send('Post with _id:'+req.params.postId+' not found!');
    }else{
      res.status(200).send(post);
    }
  } catch (error) {
    return res.status(500).send({
      error: `An internal server error has occured: ${error}`
    });
  }
}
