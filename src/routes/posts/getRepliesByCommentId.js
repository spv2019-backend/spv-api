import { Comment } from '../../models';

export default async (req, res) => {
    try {
        const comment = await Comment.findOne({_id: req.params.commentId});
        if(!comment){
            return res.status(404).send('Comment with _id:'+req.params.commentId+' not found!');
        }

        const replies = await Comment.find({'_id': { $in: comment.replies}});
        res.send(replies);

    } catch (error) {
        return res.status(500).send({
            error: `An internal server error has occured: ${error}`
        });
    }
}
