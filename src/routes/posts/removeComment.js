import {Course, Post, Comment} from "../../models";

export default async (req, res) => {
    try {
        await Comment.findOneAndDelete({_id: req.params.commentId, authorId: req.decodedAuthToken.id},
            err => {
                if (!err) {
                    res.status(204).send();
                } else {
                    res.status(401).send({
                        status: 'Unauthorized'
                    });
                }
            });

    } catch (error) {
        return res.status(500).send({
            error: `An internal server error has occured: ${error}`
        });
    }
}
