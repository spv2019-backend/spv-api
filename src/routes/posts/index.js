import { Router } from 'express';
import checkAPI from 'express-validator/check';
import multer from 'multer';
import fs from 'fs';

import getAll from './getAll';
import getById from './getById';
import createPost from './create';
import updateById from './updateById';
import { Post } from '../../models';
import auth from '../../middleware/auth';
import addComment from './addComment';
import getComment from './getCommentById';
import getCommentsByPostId from "./getCommentsByPostId";
import deleteById from './deleteById';
import deleteCommentById from './removeComment';
import updateCommentById from './updateComment';
import addReply from './addReply';
import getRepliesByCommentId from "./getRepliesByCommentId";

const posts = Router();
posts.param('postId', async (req, res, next) => {
  const post = await Post.findById(req.params.postId)

  if (post) {
    req.post = post;
    next();
  } else {
    return res.status(404).send({
      error: `Post with ID ${req.params.postId} does not exist in the database.`
    });
  }
});
// COMMENTS
// posts.get('/comment/:commentId',getComment);
posts.get('/comments/:postId', getCommentsByPostId);
posts.delete('/comments/:commentId', auth(), deleteCommentById);
posts.post('/comments/:postId', [
    checkAPI.check('text').isString()
],auth(), addComment);
posts.put('/comments/:commentId', auth(), updateCommentById);

//REPLIES
posts.post('/reply/:commentId', [
  checkAPI.check('text').isString()
],auth(), addReply);
posts.get('/reply/:commentId', getRepliesByCommentId);

// POSTS
posts.get('/', getAll);
posts.get('/:postId', getById);
posts.post('/', [
  checkAPI.check('title').isString(),
  checkAPI.check('description').isString(),
  checkAPI.check('courseId').isString()
],auth(), createPost);
posts.put('/:postId', [
  checkAPI.check('title').optional().isString(),
  checkAPI.check('description').optional().isString(),
  checkAPI.check('rating').optional().isNumeric(),
],auth(), updateById);
posts.delete('/:postId', auth(), deleteById);

export default posts;
