import {Course, Post} from "../../models";

export default async (req, res) => {
    try {
        console.log('Request is here!');
        await Post.findOneAndRemove({_id: req.params.postId, authorId: req.decodedAuthToken.id},
            err => {
                if (!err) {
                    res.status(204).send('deleted');
                } else {
                    res.status(401).send({
                        status: 'Unauthorized'
                    });
                }
            });

    } catch (error) {
        return res.status(500).send({
            error: `An internal server error has occured: ${error}`
        });
    }
}
