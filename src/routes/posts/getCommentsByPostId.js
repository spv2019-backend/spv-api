import { Post, Comment } from '../../models';

export default async (req, res) => {
    try {
        const post = await Post.findOne({_id: req.params.postId});
        if(!post){
            return res.status(404).send('Comment with _id:'+req.params.postId+' not found!');
        }

        const comments = await Comment.find({'_id': { $in: post.replies}});
        // TODO: actually fetch the replies to a max depth of 1!!!! no recursion....
        // comments.forEach(c => {
        //
        // });
        res.send(comments);

    } catch (error) {
        return res.status(500).send({
            error: `An internal server error has occured: ${error}`
        });
    }
}
