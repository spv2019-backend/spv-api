import checkAPI from 'express-validator/check';

import { Faculty, Program } from '../../models';

export default async (req, res) => {
  const validationErrors = checkAPI.validationResult(req);

  if (!validationErrors.isEmpty()) {
    return res.status(422).send({
      errors: validationErrors.array()
    });
  }

  const program = new Program(req.body);
  try {
    await program.save();
    const faculty = await Faculty.findOne({
      _id: req.params.facultyId
    });
  
    faculty.programs.push(program._id);
    await faculty.save();
  } catch (err) {
    return res.status(500).send({
      error: 'Internal server error'
    });
  }
  return res.status(204).send();
}
