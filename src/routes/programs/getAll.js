import { Program } from "../../models";

export default async (req, res) => {
  const programs = await Program.find(req.filter || null);
  return res.send(programs);
}
