import { Router } from 'express';
import checkAPI from 'express-validator/check';

import { Program } from '../../models';
import getAll from './getAll';
import getById from './getById';
import create from './create';
import filter from '../../middleware/filter';
import addCourse from './addCourse';

const programs = Router();

programs.param('programId', async (req, res, next) => {
  const program = await Program.findOne({
    _id: req.params.programId
  })

  if (program) {
    req.program = program;
    next();
  } else {
    return res.status(404).send({
      error: `Program with ID ${req.params.programId} does not exist in the database.`
    });
  }
});

programs.get('/', filter(['type']), getAll);
programs.get('/:programId', getById);

programs.post('/faculty/:facultyId', [
  checkAPI.check('type').isIn([ 'UNI', 'VS' ]),
  checkAPI.check('courses').optional().isArray()
], create)
programs.post('/:programId/courses', [
  checkAPI.check('acronym').isString(),
  checkAPI.check('name').isString(),
  checkAPI.check('year').isNumeric(),
  checkAPI.check('description').isString(),
  checkAPI.check('image').optional().isString(),
], addCourse)

export default programs;
