import checkAPI from 'express-validator/check';
import { Course } from '../../models';

export default async (req, res) => {
  const validationErrors = checkAPI.validationResult(req);

  if (!validationErrors.isEmpty()) {
    return res.status(422).send({
      errors: validationErrors.array()
    });
  }

  const course = new Course({
    program: req.program._id,
    ...req.body,
  });
  try {
    const newCourse = await course.save();
    req.program.courses.push(newCourse._id)
    await req.program.save();
  } catch(err) {
    return res.status(500).send({
      error: 'Internal server error'
    });
  }
  return res.status(204).send();
}