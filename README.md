# SPV API

The SPV API repository.

## Getting Started

Instructions to set up a local development environment.

### Prerequisites

* [Node.js (10.15.3 LTS or later)](https://nodejs.org/en/)

### Installing

Clone this repository and change directory

```sh
git clone https://gitlab.com/spv2019-backend/spv-api.git
cd spv-api
```

Install the required dependencies

```sh
npm install
```

Run the application

```sh
npm start
```

The API is now accessible on [http://localhost:8080/api/v1](http://localhost:8080)
