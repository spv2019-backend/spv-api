FROM node:10.15.3-alpine

RUN mkdir -p /api
WORKDIR /api

COPY package*.json /api/

RUN npm install

COPY . /api

EXPOSE ${EXPRESS_PORT}

CMD ["npm", "start"]